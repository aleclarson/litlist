const isObject = require('is-object')
const weaver = require('weaver')
const frame = require('framesync')
const ruley = require('ruley')
const env = require('what-env')
const $ = require('umbrella')

const noop = Function.prototype
const empty = {}

class LitList {
  constructor(opts) {
    if (!isObject(opts)) {
      throw TypeError('Expected an options object')
    }

    this._rows = new Array(opts.size || 0)
    this._root = null

    // Measurement props
    this._willMeasure = true
    if (opts.root)
      this.mount(opts.root)

    // Scroll position props
    this._offset = 0
    this._nextOffset = null
    this._willScroll = false
    this._didScroll = false

    // Rendering props
    this._willRender = false
    this._rendered = [-1, -1]
    this._contentLength = 0

    // Painting props
    this._window = {start: -1, end: -1}
    this._painted = []
    this._willPaint = false

    // Event listener props
    this._onDisplay = opts.onDisplay || noop
    this._onHide = opts.onHide || noop

    // Configure with all possible options.
    this._configure({
      padding: opts.padding || 0,
      renderRow: opts.renderRow || null,
    })

    // Enqueue a scroll position change.
    if (opts.offset > 0) {
      this.scrollTo(opts.offset)
    }

    // Begin rendering rows immediately.
    if (opts.renderRow && opts.size > 0) {
      this._display()
    }
  }

  get size() {
    return this._rows.length
  }

  get offset() {
    return this._offset
  }

  get maxOffset() {
    const padding = this._spaceBefore + this._spaceAfter
    return Math.max(0, padding + this._contentLength - this._rootLength)
  }

  get $rows() {
    return $(this._wrap.children)
  }

  get(i) {
    return this._rows[i]
  }

  find(fn) {
    const idx = this.findIndex(fn)
    return this._rows[idx]
  }

  findIndex(fn) {
    const rows = this._rows
    for (let i = 0; i < rows.length; i++) {
      const row = rows[i]
      if (fn(row, i)) {
        return i
      }
    }
    return -1
  }

  append(row) {
    if (!isObject(row)) {
      throw TypeError('Expected an object')
    }
    if (!isNumber(row.length)) {
      throw TypeError('`row.length` must be a valid number')
    }
    if (row.node) {
      rowRule.apply(row.node)
    }
    this._rows.push(row)
    this._render()
  }

  remove(first, last = first) {
    if (first >= 0 && first <= last && last < this._rows.length) {
      if (frame.step) {
        setTimeout(() => this._removeRows(first, last))
      } else {
        this._removeRows(first, last)
      }
    } else {
      throw RangeError(`Invalid range: {${first}, ${last}}`)
    }
  }

  refresh(opts = {}) {
    this._configure(opts)
    if (opts.renderRow !== undefined) {
      this._reset(opts.size || this._rows.length)
    } else if (isNumber(opts.size)) {
      const {size} = this
      if (size > opts.size) {
        return this.remove(opts.size, size - 1)
      }
      this._rows.length = opts.size
    }
  }

  scrollTo(offset) {
    this._nextOffset = offset
    this._paint()
  }

  gotoRow(idx, opts = empty) {
    this.renderFrom(idx)
    const scroll = () => {
      let offset = this._rows[idx].start
      if (opts.offset) offset += opts.offset
      this.scrollTo(offset)

      if (opts.paused != null) {
        this.pause(opts.paused)
      }
    }
    if (this._willRender) {
      weaver.high(scroll)
    } else {
      scroll()
    }
  }

  measure() {
    this._willMeasure = true
  }

  // TODO: Mark rows in the previous window as not painted.
  // TODO: Add `done` callback argument?
  renderFrom(idx) {
    if (idx < 0 || idx >= this._rows.length) {
      throw RangeError('Index out of bounds: ' + idx)
    }
    const rendered = this._rendered
    if (rendered[0] > idx || rendered[1] < idx) {
      if (idx > rendered[1] + 1) {
        this._contentLength += this._lengthSum(idx - 1, rendered[1] + 1)
      }
      rendered[0] = idx, rendered[1] = idx
      this._forceRender()
    }
  }

  pause(paused) {
    if (arguments.length) {
      if (typeof paused != 'boolean') {
        throw TypeError('Expected a boolean')
      }
      this._paused = paused
      if (!paused) this._render()
    } else {
      this._paused = true
    }
  }

  unpause() {
    if (this._paused) {
      this._paused = false
      this._render()
    }
  }

  mount(root) {
    if (this._root)
      throw Error('Cannot mount more than once')
    if (!root || root.nodeType != 1)
      throw TypeError('Expected an element node')

    this._root = root
    this._rootStyle = getComputedStyle(root)
    this._rootLength = null
    this._spaceBefore = null
    this._spaceAfter = null

    // The root element must have a relative position
    // for the rows to be absolutely positioned within.
    listRule.apply(root)

    // Repaint in reaction to scroll events.
    $(root).on('scroll', () => {
      if (this._willScroll) {
        this._willScroll = false
      } else if (!this._didScroll) {
        this._didScroll = true
        this._paint()
      }
    })

    // The "wrap" preserves content length
    // and provides spacing after the content.
    const wrap = $.elem('div')
    root.appendChild(wrap)
    wrapRule.apply(wrap)
    this._wrap = wrap

    // Try to render.
    this._render()
  }

  _configure(config) {
    for (let key in config) {
      if (configurable.hasOwnProperty(key)) {
        const value = config[key]
        if (value !== undefined) {
          configurable[key].call(this, value)
        }
      }
    }
  }

  _reset(size) {
    this._rows = new Array(size || 0)
    this._window.start = -1
    this._window.end = -1
    this._painted.length = 0
    this._rendered[0] = -1
    this._rendered[1] = -1
    this._forceRender()
  }

  _measure(done) {
    const style = this._rootStyle

    // The viewport length.
    let length = style.height

    // Wait for the length to be measurable.
    if (!length) {
      const retry = () => this._measure(done)
      return frame.once('end', () => setTimeout(retry), true)
    }
    length = parseFloat(length)

    // Root padding is used for row positioning.
    this._spaceBefore = parseFloat(style.paddingTop || 0)
    this._spaceAfter = parseFloat(style.paddingBottom || 0)

    // The computed height is fucked up for flex containers.
    if (style.display.indexOf('flex') >= 0)
      length += this._spaceBefore + this._spaceAfter

    this._rootLength = length
    done()
  }

  _render(force) {
    if (this._willRender) return

    // Cannot render if not mounted.
    if (!this._root) return

    // Cannot render if paused.
    if (!force && this._paused) return

    // Cannot render if no rows exist.
    const maxIndex = this._rows.length - 1
    if (maxIndex < 0) return

    // All rows may be rendered.
    const rendered = this._rendered
    if (rendered[0] == 0 && rendered[1] == maxIndex) return
    this._willRender = true

    const render = () => {
      if (!force && this._paused) {
        return this._willRender = false
      }

      // Rows may have been added/removed during the `_measure` call.
      const maxIndex = this._rows.length - 1
      if (maxIndex < 0) {
        return this._willRender = false
      }

      let first = rendered[0], last = rendered[1]
      let maxLength = this._rootLength

      // Render from beginning.
      if (first < 0) first = 0

      // Render from arbitrary index.
      else if (first == last) last -= 1

      // Render backwards.
      if (first > 0) {
        first = this._renderRows(first - 1, maxLength, -1)
      }

      // Render forwards.
      if (last < maxIndex) {
        if (last < 0) maxLength -= this._spaceBefore
        last = this._renderRows(last + 1, maxLength, 1)
      }

      rendered[0] = first
      rendered[1] = last

      // Rendering finished.
      this._willRender = false

      // Repaint in case a rendered row is within the paint window.
      this._paint()
    }

    const enqueue = rendered[0] == rendered[1] ? weaver.high : weaver.low
    if (this._willMeasure) {
      enqueue(done => {
        this._measure(() => {
          this._willMeasure = false
          render()
          done()
        })
      })
    } else {
      enqueue(render)
    }
  }

  _renderRows(first, maxLength, direction) {
    const rows = this._rows, renderRow = this._renderRow
    const lastIndex = direction > 0 ? rows.length - 1 : 0

    // Rows are positioned using this offset.
    let offset = this._computeRowStart(first)
    if (direction < 0) offset += rows[first].length

    // Rows are inserted together.
    const nodes = document.createDocumentFragment()

    let i = first, len = 0, row = null
    while (true) {
      row = rows[i]
      if (!row) {
        rows[i] = row = {}
      }
      if (!row.node) {
        row.node = renderRow(row, i)
        rowRule.apply(row.node)
      }
      if (!isNumber(row.length)) {
        throw TypeError('`row.length` must be a valid number')
      }

      if (direction > 0) row.start = offset
      offset += row.length * direction
      if (direction < 0) row.start = offset

      row.painted = false
      row.node.style.top = row.start + 'px'
      row.node.style.height = row.length + 'px'
      if (direction > 0) {
        nodes.appendChild(row.node)
      } else {
        nodes.insertBefore(row.node, nodes.firstChild)
      }

      len += row.length
      if (len < maxLength && direction * (lastIndex - i)) {
        i += direction
      } else break
    }

    const rendered = this._rendered
    if (direction > 0) {
      len = offset - this._spaceBefore
      if (len > this._contentLength) {
        this._contentLength = len
      }
      this._wrap.appendChild(nodes)
    } else {
      row = rows[first + 1]
      if (row && row.start != null) {
        this._wrap.insertBefore(nodes, row.node)
      } else {
        // TODO: Avoid using `appendChild` if nodes after the rendered range are
        // already mounted (which can happen if `renderFrom` was used before).
        this._wrap.appendChild(nodes)
      }
    }

    // Return the last rendered index.
    return i
  }

  // Compute the offset at which a row begins.
  // Do not call this when the row has a cached `start` property.
  _computeRowStart(idx) {
    if (idx == 0) {
      return this._spaceBefore
    }

    let first = this._rendered[0]
    let last = this._rendered[1]

    // Rendering from arbitrary index.
    if (first == last) {
      const start = this._spaceBefore + this._contentLength

      // Rendering forwards.
      if (idx == first) return start

      // Rendering backwards.
      if (idx == first - 1) return start - this._rows[idx].length
    }

    // The paint window is right before this index.
    else if (last == idx - 1) {
      last = this._rows[last]
      return last.start + last.length
    }

    // The paint window is right after this index.
    else if (first == idx + 1) {
      return this._rows[first].start - this._rows[idx].length
    }

    throw Error('Failed to compute row start: ' + idx)
  }

  // Abort the current render, then render even if paused.
  _forceRender() {
    if (this._willPaint) return
    if (this._willRender) {
      const paused = this._paused
      if (!paused) this._paused = true
      weaver.high(() => {
        if (paused) {
          this._render(true)
        } else {
          this._paused = false
          this._render()
        }
      })
    } else {
      this._render(true)
    }
  }

  _paint() {
    if (this._willPaint) return
    this._willPaint = true

    if (this._rendered[0] < 0) {
      throw Error('Cannot paint before rendering')
    }

    const offset = this._offset
    frame.once('render', () => {
      // Update the next scroll position.
      this._scroll()

      // Update the scrollable length.
      this._resizeContent()

      // Refresh the paint window.
      this._paintSync()

      // Images are not rendered unless the windowing range
      // is updated *before* mutating the DOM scroll position.
      if (this._willScroll) this._root.scrollTop = this._offset

      // Painting finished.
      this._willPaint = false

      // Trigger the next render.
      this._render()
    }, true)
  }

  _scroll() {
    if (this._didScroll) {
      this._offset = this._root.scrollTop
      this._didScroll = false
    }
    let offset = this._nextOffset
    if (offset !== null) {
      offset = Math.max(0, Math.min(this.maxOffset, offset))
      this._nextOffset = null
      if (this._offset != offset) {
        this._offset = offset
        this._willScroll = true
      }
    }
  }

  _resizeContent() {
    const wrapHeight =
      this._contentLength + this._spaceBefore + this._spaceAfter

    if (wrapHeight != this._wrapHeight) {
      this._wrapHeight = wrapHeight
      this._wrap.style.height = wrapHeight + 'px'
    }
  }

  // Synchronously paint all visible rows (if necessary).
  _paintSync() {
    const offset = this._offset
    const padding = this._padding()

    // The last row ends here
    const contentEnd = this._spaceBefore + this._contentLength

    // Length of the next paint window
    const length = this._rootLength + 2 * padding

    // Boundaries of the next paint window
    const start = Math.max(0, offset - padding)
    const end = Math.min(start + length, contentEnd)

    const win = this._window
    if (!win.dirty && win.start == start && win.end == end) return

    const painted = this._painted
    if (painted.length) {
      const delta = start - win.start
      if (delta) {
        this._paintFast(delta, start, end)
      } else {
        this._paintAgain(start, end)
      }
    } else {
      win.start = start
      win.end = end
      painted[0] = this._findRow(0, start, 1)
      painted[1] = this._displayRows(painted[0], end, 1)
    }
  }

  // Paint in reaction to a scroll event.
  _paintFast(delta, start, end) {
    const win = this._window
    let changed = false

    const rows = this._rows
    const painted = this._painted
    let row = null, first = painted[0], last = painted[1]

    // The entire window is defunct.
    if (Math.abs(delta) >= win.end - win.start) {
      let i = first; do {
        this._hideRow(rows[i], i)
      } while (++i <= last)

      changed = true
      if (delta > 0) {
        const contentEnd = this._spaceBefore + this._contentLength
        if (start - win.end < contentEnd - end) {
          first = this._findRow(last, start, 1)
          last = this._displayRows(first, end, 1)
        } else {
          last = this._findRow(this._rendered[1], end, -1)
          first = this._displayRows(last, start, -1)
        }
      } else {
        if (start < win.start - end) {
          first = this._findRow(0, start, 1)
          last = this._displayRows(first, end, 1)
        } else {
          last = this._findRow(first, end, -1)
          first = this._displayRows(last, start, -1)
        }
      }
    }

    // Part of the window is still visible.
    else if (delta > 0) {
      row = rows[first]
      if (start > row.start + row.length) {
        first = this._hideRows(first, start, 1)
        changed = true
      }
      row = rows[last]
      if (end > row.start + row.length) {
        last = this._displayRows(last, end, 1)
        changed = true
      }
    } else {
      row = rows[last]
      if (end < row.start) {
        last = this._hideRows(last, end, -1)
        changed = true
      }
      row = rows[first]
      if (start < row.start) {
        first = this._displayRows(first, start, -1)
        changed = true
      }
    }

    // Update the window and its painted range.
    if (changed) {
      win.start = start, win.end = end
      painted[0] = first, painted[1] = last
    }
  }

  // Repaint the same offset with an optionally larger window.
  _paintAgain(start, end) {
    const win = this._window, painted = this._painted,
      first = painted[0], last = painted[1]

    if (start < win.start) {
      painted[0] = this._displayRows(first, start, -1)
    } else if (start > win.start) {
      throw Error('Must call `_reset` for smaller windows')
    }

    // Check for inserted rows.
    if (win.dirty) {
      win.dirty = false
      const rows = this._rows
      let i = first; do {
        let row = rows[i]
        if (!row.painted) {
          this._displayRow(row, i)
        }
      } while (++i <= last)
    }

    if (end > win.end) {
      painted[1] = this._displayRows(last, end, 1)
    } else if (end < win.end) {
      throw Error('Must call `_reset` for smaller windows')
    }

    if (first != painted[0] || last != painted[1]) {
      win.start = start, win.end = end
    }
  }

  _removeRows(first, last) {
    const wrap = this._wrap
    const rows = this._rows
    const max = this._rendered[1]

    let i = first, row, len = 0
    do {
      row = rows[i]
      if (wrap == row.node.parentNode) {
        wrap.removeChild(row.node)
        len += row.length
      }
    } while (++i <= last)

    this._contentLength -= len

    if (last == max) {
      rows.length = first
    } else {
      // Update the start offsets of any rows after.
      while (++i <= max) {rows[i].start -= len}
      rows.splice(first, 1 + last - first)
    }

    // Update the cached scroll offset.
    const {maxOffset} = this
    if (this._offset > maxOffset) {
      this._offset = maxOffset
    }

    // Refresh the window if removing visible rows.
    const painted = this._painted
    if (painted.length && painted[0] <= last && painted[1] >= first) {
      // TODO: Support removing visible rows.
      throw Error('Unimplemented')
    }
  }

  _findRow(first, maxLength, direction) {
    const rows = this._rows

    let i = first, row = rows[i]
    let len = row.start, last = 0
    if (direction > 0) {
      last = this._rendered[1]
    } else {
      len += row.length
    }

    while (i != last) {
      len += row.length * direction
      if (direction * (maxLength - len) > 0) {
        row = rows[i += direction]
      } else break
    }
    return i
  }

  _lengthSum(first, last) {
    const rows = this._rows
    const direction = last > first ? 1 : -1

    let len = 0, i = first
    while (direction * (last - i) >= 0) {
      len += rows[i].length, i += direction
    }
    return len
  }

  _displayRow(row, i) {
    row.painted = true
    if (row.onDisplay) row.onDisplay()
    this._onDisplay(row, i)
  }

  _hideRow(row, i) {
    row.painted = false
    if (row.onHide) row.onHide()
    this._onHide(row, i)
  }

  _displayRows(first, maxLength, direction) {
    const rows = this._rows

    let i = first, row = rows[i]
    let len = row.start, lastIndex = 0
    if (direction > 0) {
      lastIndex = this._rendered[1]
    } else {
      len += row.length
    }

    while (true) {
      if (!row.painted) {
        this._displayRow(row, i)
      }
      if (i == lastIndex) break
      len += row.length * direction
      if (direction * (maxLength - len) > 0) {
        row = rows[i += direction]
      } else break
    }

    // Return the last visible index.
    return i
  }

  _hideRows(first, maxLength, direction) {
    const rows = this._rows
    let i = first, row = rows[i], len = row.start
    if (direction < 0) len += row.length
    do {
      len += row.length * direction
      if (direction * (maxLength - len) > 0) {
        if (row.painted) this._hideRow(row, i)
        row = rows[i += direction]
      } else return i
    } while (row)
  }
}

module.exports = LitList

// Anonymous styles
const listRule = ruley('position: relative; overflow-y: auto;')
const rowRule = ruley('position: absolute; left: 0; width: 100%;')
const wrapRule = ruley('position: absolute; top: 0; left: 0; width: 100%;')

if (env.css('contain')) {
  rowRule.style.contain = 'strict'
  wrapRule.style.contain = 'strict'
}

// Options whose values are validated and/or reacted to.
const configurable = {
  padding(value) {
    if (typeof value == 'string') {
      const p = parseFloat(value) / 100
      this._padding = () => p * this._rootLength
    } else {
      const p = isNumber(value) ? value : 0
      this._padding = () => p
    }
  },
  renderRow(value) {
    if (typeof value == 'function') {
      this._renderRow = value
    } else {
      this._renderRow = null
    }
  },
}

function isNumber(x) {
  return typeof x == 'number' && !isNaN(x)
}
