
# litlist v0.0.1

Row caching and windowing for performant lists. 🔥

```js
import List from 'litlist'

const list = new List({
  container: document.querySelector('#my-list')
})
```

### How windowing works

Rows not within the container's viewport have `visibility: hidden`
added to their inline style. This seems to be faster than mutating
the DOM tree on-demand.

This means all rows are inserted into the DOM (regardless of visibility),
which helps with row measurement and accurate scrollbar length.
In the future, there will be a maximum number of elements in the DOM
that you can customize.

Use the `padding` option to show elements beyond the viewport
preemptively. The padding is in pixels.

### Rendering rows

Rendering rows is simple. You have two choices:

- Append each row manually
- Generate each row until `rowCount` is met

```js
// Append a single row.
list.append({
  node: document.createElement('div'),
  length: 40,
})

// Generate the rows using `renderRow`.
list.refresh({
  renderRow(i, row) {
    // Reuse the cached node.
    if (row.node) return row.node

    // Otherwise, create a new node.
    row.length = 40
    return document.createElement('div')
  }
})
```

### Removing rows

Currently, you can only remove all rows or any number of rows at the end
of the list.

```js
// Remove the last 2 rows.
list.refresh({
  rowCount: list.rowCount - 2,
})

// Remove all rows.
list.refresh({
  rowCount: 0,
})
```

In the future, a `remove` method will be added for removing a single row.

### Reordering rows

In the near future, it will be easy to add reordering to your list.
